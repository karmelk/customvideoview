package com.example.karen.customvideoview;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.videoviewlibrary.CustomLayout;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CustomLayout customLayout= (CustomLayout) findViewById(R.id.custom_layout);
        customLayout.initialize(this,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4");
        customLayout.enableAudioMode(true);
        customLayout.enableMediaController(true);
        customLayout.enableRotateIconMode(true);

    }

}
