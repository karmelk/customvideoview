package com.example.videoviewlibrary;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by Karen on 08.01.2018.
 */

public class CustomVideoView extends VideoView {
    private Context mContext;
    private ProgressDialog videoViewDialog;
    private MediaController mediaController;
    private String mPath;
    private CustomLayout mCustomLayout;

    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomVideoView(Context context) {
        super(context);
    }

    @Override
    public void setOnPreparedListener(MediaPlayer.OnPreparedListener l) {
        super.setOnPreparedListener(l);
    }

    public void initialize(final Context context, String path, CustomLayout customLayout) {
        mContext = context;
        mPath = path;
        mCustomLayout = customLayout;

        if (mediaController == null) {
            mediaController = new MediaController(mContext);
        }

        initVideo();
    }

    @Override
    public void setVideoPath(String path) {
        super.setVideoPath(path);
        setUpDialog();
    }

    private void initVideo() {
        CustomVideoView.this.setBackgroundColor(Color.WHITE);
        setVideoPath(mPath);
        setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                CustomVideoView.this.setBackgroundColor(Color.TRANSPARENT);
                start();
                mCustomLayout.showAudioIcon();
                videoViewDialog.dismiss();
                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                        if (mCustomLayout.mediaControlerStatus == true) {
                            setMediaController(mediaController);
                            mediaController.setAnchorView(CustomVideoView.this);
                        }
                        mCustomLayout.showAnimationImageView();
                    }
                });
            }
        });
    }

    private void setUpDialog() {
        videoViewDialog = new ProgressDialog(mContext);
        videoViewDialog.setTitle("Stream video");
        videoViewDialog.setMessage("Buffering");
        videoViewDialog.setCancelable(false);
        videoViewDialog.show();
    }

}
