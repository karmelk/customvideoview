package com.example.videoviewlibrary;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Karen on 11.01.2018.
 */

public class CustomLayout extends FrameLayout {
    private boolean volumeStatus = false;
    private AudioManager mAudioManager;
    private ImageView mAudioMute;
    private ImageView mRotate;
    private Context mContext;
    private CustomVideoView customVideoView;
    private boolean audioModeStatus;
    private boolean rotateIconStatus;
    protected boolean mediaControlerStatus;
    private Animation animation;
    private SharedPreferences preferences;

    public CustomLayout(@NonNull Context context) {
        super(context);
    }

    public CustomLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void initialize(Context context, String path) {
        mContext = context;
        customVideoView = new CustomVideoView(context);
        customVideoView.initialize(context, path, CustomLayout.this);
        CustomLayout.this.addView(customVideoView);
    }

    public void enableAudioMode(boolean audioMode) {
        if (audioMode == true)
            audioModeStatus = true;
    }

    protected void showAudioIcon() {
        if (audioModeStatus == true)
            CustomLayout.this.addView(audioMode(mContext));
    }

    public void enableMediaController(boolean controllerMode) {
        if (controllerMode == true)
            mediaControlerStatus = true;
    }


    public void enableRotateIconMode(boolean rotateIconMode) {
        if (rotateIconMode==true)
            rotateIconStatus = true;
    }

    protected void showAnimationImageView() {
        if (rotateIconStatus == true)
            CustomLayout.this.addView(animateImageView());
    }


    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(mContext, "ORIENTATION_LANDSCAPE", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(mContext, "ORIENTATION_PORTRAIT", Toast.LENGTH_SHORT).show();
        }
    }

    private ImageView audioMode(final Context context) {
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        preferences = mContext.getSharedPreferences("", MODE_PRIVATE);
        mAudioMute = new ImageView(context);
        boolean audioStat = preferences.getBoolean("audioStat", false);

        if (audioStat == true) {
            mAudioMute.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_volume_up_white_24dp));
            volumeStatus = true;
        } else {
            mAudioMute.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_volume_off_white_24dp));
            volumeStatus = false;
        }

        FrameLayout.LayoutParams paramsAudioMute = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsAudioMute.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        paramsAudioMute.setMargins(0, 0, 16, 16);
        mAudioMute.setLayoutParams(paramsAudioMute);
        mAudioMute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (volumeStatus == true) {
                    sharedAudioStat(false);
                    mAudioMute.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_volume_off_white_24dp));
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                    volumeStatus = false;
                } else {
                    sharedAudioStat(true);
                    mAudioMute.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_volume_up_white_24dp));
                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 100, 0);
                    volumeStatus = true;
                }
            }
        });
        return mAudioMute;
    }


    private ImageView animateImageView() {
        FrameLayout.LayoutParams paramsanimate = new FrameLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsanimate.gravity = Gravity.TOP | Gravity.LEFT;
        paramsanimate.setMargins(16, 16, 0, 0);
        mRotate = new ImageView(mContext);
        mRotate.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_screen_rotation_white_24dp));
        mRotate.setLayoutParams(paramsanimate);

        animation = AnimationUtils.loadAnimation(mContext,
                R.anim.rotate);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mRotate.setVisibility(View.VISIBLE);
        mRotate.startAnimation(animation);

        return mRotate;
    }

    private void sharedAudioStat(boolean audioModeStat) {
        preferences = mContext.getSharedPreferences("", MODE_PRIVATE);
        SharedPreferences.Editor ed = preferences.edit();
        ed.putBoolean("audioStat", audioModeStat);
        ed.commit();
        Log.i(TAG, "audioMode: put" + audioModeStat);

    }

}
